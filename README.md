# tsabn publicly available R package

## Description

tsabn is an R package that extends abn R package for time series analysis. This is a machine learning approach to empirically identifying associations in complex and high dimensional datasets of time series. 

In recent years, Additive Bayesian Networks (ABN) analysis has been successfully used in many fields from sociology to veterinary epidemiology. This approach has shown to be very efficient in embracing the correlated multivariate nature of high dimensional datasets and in producing data driven model instead of expert based models. ABN is a multidimensional regression model analogous to generalised linear modelling but with all variables as potential predictors. The final goal is to construct the Bayesian network that best support the data. When applying ABN to time series dataset it is of high importance to cope with the autocorrelation structure of the variance-covariance matrix as the structural learning process relies on the estimation of the relative quality of the model for the given dataset. This is done by using common model selection score such as AIC or BIC. We adapt the ABN framework such that it can handle time series and longitudinal datasets and then generalize the time series regression for a given set of data. We implement an iterative Cochrane-Orcutt procedure in the fitting algorithm to deal with serially correlated errors and cope with the between- and within- cluster effect in regressing centred responses over centred covariate.

## Installation

To install tsabn it can required to have a devel version of abn (please email me: gilles.kartzer@math.uzh.ch)
install.packages("https://git.math.uzh.ch/reinhard.furrer/tsabn/raw/master/tsabn_0.1.tar.gz", repo=NULL, type="source")

## Future implementations (ordered by urgency)

 * buildscorecahce.mle.ts(): implementation of ban and retain matrix; defn.res user provided; dry.run; centre option;
 * setup common sens test for fitabn.mle.ts() and buildscorecache.mle.ts()